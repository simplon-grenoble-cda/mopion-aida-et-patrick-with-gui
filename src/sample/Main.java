package sample;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Scanner;

public class Main extends Application {
    private Stage primaryStage ;
    private Scene scene1;
    private Scene scene2;
    //GUI variables
    private GridPane gridPane = new GridPane();
    private BorderPane borderPane = new BorderPane();
    private BorderPane borderPane1 = new BorderPane();
    private HBox hbox = new HBox();
    private HBox hbox1 = new HBox();
    private HBox hbox2 = new HBox();
    private Label title = new Label("Tic Tac Toe");
    private VBox vbox1 = new VBox();
    private VBox vbox = new VBox();
    private Button restartGame = new Button("Restart Game");
    private Label namePlayer1T = new Label("Nom de joueur 1");
    private Label namePlayer2T = new Label("Nom de joueur 2");
    private TextField namePlayer1 = new TextField();
    private TextField namePlayer2 = new TextField();
    //les 8 button
    private Button [] btns = new Button[9];
    // création des joueurs
    Player player1 = new Player(1, 'x');
    Player player2 = new Player(2, 'o');
    Player currentPlayer = player1;
    private Label labelNamePlayer = new Label(" ");

    // Grille
    Grid myGrid = new Grid();






    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage=primaryStage;

        //Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        this.createGUI();

        //transmet adverssaire
       // player1.setOpponent(player2);
        //player2.setOpponent(player1);
       this.idButtonClicked();
        this.restartButton();
        primaryStage.setTitle("Tic Tac Toe");
        scene1 = new Scene(borderPane1, 300, 250);
        scene2 = new Scene(borderPane, 600, 500);
        hbox1.getChildren().add(namePlayer1T);
        hbox1.getChildren().add(namePlayer1);
        hbox2.getChildren().add(namePlayer2T);
        hbox2.getChildren().add(namePlayer2);
        vbox1.getChildren().add(hbox1);
        vbox1.getChildren().add(hbox2);
        borderPane1.setCenter(vbox1);
        Button go1 = new Button();
        borderPane1.setBottom(go1);

        primaryStage.setScene(scene1);

        go1.setText("aller jouer");
        go1.setOnAction((e)->onStart());
        primaryStage.show();

    }
    private void onStart(){
        currentPlayer =player1;
        player1.name=namePlayer1.getText();
        player2.name=namePlayer2.getText();

        labelNamePlayer.setText("le joueur"+" "+currentPlayer.name+" "+"joue");

        primaryStage.setScene(scene2);

    }
//fonction pour créer GUI
    private void createGUI() {
        hbox.getChildren().add(title);
        vbox.getChildren().add(hbox);
        vbox.getChildren().add(labelNamePlayer);

        hbox.setAlignment(Pos.CENTER);
        vbox.setAlignment(Pos.CENTER);
        hbox.setPadding(new Insets(10,10,10,10));
        //borderPane.setTop(hbox);

        borderPane.setTop(vbox);
        borderPane.setBottom(restartGame);

        borderPane.setAlignment( restartGame, Pos.CENTER);
        borderPane.setPadding(new Insets(20,20,20,20)  );


        //création des 9 bouttons
       int label=0;

        for (int i = 0; i <3; i++) {
            for (int j = 0; j <3; j++) {
                Button button = new Button(Integer.toString(label));
                button.setId(Integer.toString(label));
                gridPane.add(button,j,i);
                gridPane.setAlignment(Pos.CENTER);
                gridPane.setPadding(new Insets(10,10,10,10));
                button.setPrefHeight(150);
                button.setPrefWidth(150);
                btns[label]=button;
                label++;

            }
        }
        borderPane.setCenter(gridPane);
    }

    private void restartButton() {
        //restart button click

        restartGame.setOnAction((e) -> {
            myGrid = new Grid();
            System.out.println("hy");
            for(int i=0 ;i<9;i++){
                btns[i].setText(i+" ");

            }
            primaryStage.setScene(scene1);
        });
    }

    private void idButtonClicked(){


        for(Button btn:btns){
            btn.setOnAction((e)-> onClicked(btn));
        }

    }
    public void onClicked(Button btn ){
        Integer idI = Integer.valueOf(btn.getId());
        idI++;
        System.out.println("idI" + idI);
        if(myGrid.checkCase(idI)) {
            myGrid.setGame(idI , currentPlayer);
            char jeton = currentPlayer.jeton;
            String jetonS = String.valueOf(jeton);
            btn.setText(jetonS);

            if( myGrid.checkWinner()){
                Alert a = new Alert(Alert.AlertType.NONE);
                a.setAlertType(Alert.AlertType.WARNING);
                a.setContentText("bravo"+" "+currentPlayer.name);
                a.show();
            }
            if( myGrid.checkFull()){
                Alert a = new Alert(Alert.AlertType.NONE);
                a.setAlertType(Alert.AlertType.WARNING);
                a.setContentText("égalité");
                a.show();
            }

            permutation();

        }
        else{
            //btn.setStyle("-fx-background-color: red;");
            //Alert a = new Alert(Alert.AlertType.NONE);
            //alert
            //a.setAlertType(Alert.AlertType.WARNING);
            //alert personaliser
            //a.setContentText("la case est déja rempli");
            //a.show();
        }


    }
    public void permutation(){

        if (currentPlayer == player1) {
            currentPlayer= player2;

        } else {
            currentPlayer = player1;
        }


        labelNamePlayer.setText("le joueur"+" "+currentPlayer.name+" "+"joue");
    }







    public static void main(String[] args) {
        launch(args);
    }
}
