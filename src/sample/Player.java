package sample;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Player {
    public String name;
    public char jeton;
    public int num;
    public boolean checkIA;
    private Player opponent;
    private int QI;

    public Player(int num, char jeton) {
        // numéro du joueur (1 ou 2)
        this.num = num;
        // nom du joueur
        this.name = "";
        // char qui apparait dans la grille ( 'X' ou 'O' )
        this.jeton = jeton;
        // boolean true si joueur IA
        this.checkIA = false;
        // intélligence de l'IA - de 1 à 9 - nombre de tours calculés
        this.QI = 3;
    }

    /**
     * référence au joueur adverse >> appel obligatoire !
     * @param opponent  adversaire
     */
    public void setOpponent(Player opponent) {
        // référence au joueur adverse
        this.opponent = opponent;
    }

    /**
     * Demande du nom à l'utilisateur
     */
//    public void getUserNameInput (){
//        boolean inputValable = false;
//        String userProp = "";
//
//        while (!inputValable) {
//            Scanner scanner = new Scanner(System.in);
//            try {
//                System.out.print("Joueur numéro "+this.num+" :  ");
//                userProp = scanner.nextLine();
//
//                inputValable = true;
//
//            } catch (InputMismatchException exception) {
//                System.out.println("Entrée non valide  :(");
//            }
//            System.out.println();
//        }
//
//        // Saisie OK >> a-t-on à faire à une IA ?
//        if (userProp.equalsIgnoreCase("IA")) {
//            this.checkIA = true;
//            this.name = "IA";
//        } else {
//            // enregistrement du joueur
//            this.name = userProp;
//        }
//    }

    /**  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> IA
     * Fait jouer l'IA
     * @param myGrid    grille de jeu
     */
    public void playIA(Grid myGrid) {

        // Demande quel est la meilleur coup à jouer
        int [] bestPlay = new int[2];
        int myQI = Math.min(QI, myGrid.nbrStrokes);
        bestPlay = searchBestPlay(myGrid, myQI);

        // récupère le numéro de la case et le score associé
        int numCasePlay = bestPlay[0];
        //int bestScore = bestPlay[1];

        // Out screen
        System.out.println("L'ordinateur [" + this.jeton + "] a joué : " + numCasePlay);
        System.out.println();

        // joue
        myGrid.setGame(numCasePlay, this);
    }

    /**
     * Test toutes les possibités de jeu et retourne le meilleur coup
     * @param myGrid    grille de jeu
     * @return      int[2]  >>  [0] = numéro de case   [1] = score obtenu pour cette case
     */
    public int[] searchBestPlay(Grid myGrid, int myQI) {
        int numCasePlay = 0;
        int bestScore = -100;

        for (int I=1 ; I<=9 ; I++) {
            // test toutes les cases libres
            if (myGrid.checkCase(I)) {
                // créé une nouvelle grille
                Grid testGrid = myGrid.clone();
                // Joue le coup en cours dans la nouvelle grille
                testGrid.setGame(I,this);
                // Demande le score du résultat
                int score = testGrid.getScore(this);

                // si le QI en cours nous le permet
                if (myQI > 1) {
                    // Test le coup suivant (tous les coups de l'autre joueur)
                    int[] resultNextPlay = opponent.searchBestPlay(testGrid, (myQI-1));
                    // retire le score du tour suivant à notre score
                    score -= resultNextPlay[1];
                }

                // trace le premier niveau
                /*if (myQI == this.QI || myQI == myGrid.nbrStrokes) {
                    System.out.println(I+" >>> "+score);
                }*/

                // extrait le meilleur score
                if (score > bestScore) {
                    numCasePlay = I;
                    bestScore = score;
                }
            }
        }

        int[] varReturn = {numCasePlay, bestScore};
        return varReturn;
    }
}


