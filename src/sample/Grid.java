package sample;


import java.util.InputMismatchException;
import java.util.Scanner;

public class Grid implements Cloneable {

    private Player[][] matrix;
    private Player noPlayer;
    public int nbrStrokes;

    public Grid () {
        // matrice de joueurs représentant la grille
        this.matrix = new Player[3][3];
        // référence au joueur d'une case non jouée
        this.noPlayer = new Player(0,' ');
        // nombre de tours restant avant la fin du jeu
        this.nbrStrokes = 9;

        // rempli la grille de noPlayer
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                matrix[x][y] = noPlayer;
            }
        }
    }

    /**
     * Méthodes de clonage >> créé une copie de Grid
     * @return  retourne la nouvelle grille
     */
    public Grid clone() {
        Grid returnGrid = new Grid();

        // clone les attributs
        returnGrid.nbrStrokes = this.nbrStrokes;
        returnGrid.copyMatrix(this.matrix);
        return returnGrid;
    }

    public void copyMatrix(Player[][] inputMatrix) {
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                matrix[x][y] = inputMatrix[x][y];
            }
        }
    }

    /**
     * Affichage de la grille >> GUI
     */
    public void displayGrid () {
        int i = 0;

        for (int y=0 ; y<3 ; y++) {
            for (int x=0 ; x<3 ; x++) {
                i++;
                Player myPlayer = matrix[x][y];
                System.out.print(i +":" + myPlayer.jeton + "  ");
            }
            System.out.println();
        }
    }

    public void getUserInput(Player activPlayer) {
        boolean inputValable = false;
        int userProp = 0;
        //boolean keepPlaying = false;

        while (!inputValable) {
            Scanner scanner = new Scanner(System.in);
            try {
                System.out.print(activPlayer.name+" ["+activPlayer.jeton+"] : Que jouez-vous ?  >>>  ");
                userProp = scanner.nextInt();

                inputValable = true;

                // numero de case valide ?
                if ((userProp < 1) || (userProp>10)) {

                    System.out.println("Numéro de case invalide (1 à 9).");
                    inputValable = false;

                } else if (!checkCase(userProp)){
                    // case déjà jouée ?
                    System.out.println("Cette case est déjà remplie.");
                    inputValable = false;
                }

            } catch(InputMismatchException exception) {
                System.out.println("Entrée non valide  :(");
            }

            // ajoute une ligne vide
            System.out.println();
        }

        // mise à jour du jeu
        setGame(userProp,activPlayer);
    }

    /**
     * retourne true si la case est libre, false si elle est déjà jouée
     * @param numCase  numéro de la case à tester
     * @return true > libre   false > pas libre
     */
    public boolean checkCase(int numCase) {
        Player casePlayer = getCasePlayer(numCase);
        return (casePlayer.num == 0);
    }

    /**
     * Appelé lorsque le joueur a joué >> Met à jour la matrice
     * @param numCase   numéro de la case nbrStrokesjouée de 1 à 9
     * @param activPlayer    joueur qui a joué
     * @return true si ok, false si la case est déjà occupée
     */
    public void setGame(int numCase, Player activPlayer) {
        // on récupère les coordonnées de la case
        int[] coord = getCaseCoord(numCase);
        int x = coord[0];
        int y = coord[1];

        // place le joueur dans la case
        matrix[x][y] = activPlayer;

        // décrémente nbrStrokes
        nbrStrokes --;
    }

    /**
     * retourne les coordonnées x y à partir d'un numéro de case
     * @param numCase  numéro de la case de 1 à 9
     * @return  tableau de 2 valeurs contenant x et y
     */
    private int[] getCaseCoord (int numCase) {
        int x = (numCase-1) % 3;
        int y = (numCase-1) / 3;

        int[] valReturn = {x,y};
        return valReturn;
    }

    /**
     * retourne le joueur qui a joué la case (ou noPlayer si case non jouée)
     * @param numCase  numéro de la case de 1 à 9
     * @return   le joueur qui occupe la case
     */
    private Player getCasePlayer (int numCase) {
        // on récupère les coordonnées de la case
        int[] coord = getCaseCoord(numCase);
        int x = coord[0];
        int y = coord[1];

        return matrix[x][y];
    }

    /**  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> IA
     * Calcul le score de la grille pour le joueur activPlayer
     * @param activPlayer   Joueur auquel on co,pte les points
     * @return     score de lq grille
     */
    public int getScore(Player activPlayer) {
        int score = 0;

        // 1 >> test les lignes
        for (int x = 0; x < 3; x++) {
            score += getLineScore(activPlayer, matrix[x][0], matrix[x][1], matrix[x][2]);
        }

        // 2 >> test les colonnes
        for (int y = 0; y < 3; y++) {
            score += getLineScore(activPlayer, matrix[0][y], matrix[1][y], matrix[2][y]);
        }

        // 3 >> test les diagonales
        score += getLineScore(activPlayer, matrix[0][0], matrix[1][1], matrix[2][2]);
        score += getLineScore(activPlayer, matrix[2][0], matrix[1][1], matrix[0][2]);

        return score;
    }

    private int getLineScore(Player activPlayer, Player player1, Player player2, Player player3) {
        int score = 0;

        score += getCaseScore(activPlayer, player1);
        score += getCaseScore(activPlayer, player2);
        score += getCaseScore(activPlayer, player3);

        return (int)Math.pow(score,3);
    }

    private int getCaseScore(Player activPlayer, Player casePlayer) {
        if (activPlayer.num == casePlayer.num) {
            return 1;
        } else if (casePlayer.num == 0) {
            return 0;
        } else {
            return -1;
        }
    }

    /**
     * Vérifie si on a un gagnant
     * @return      true si on a un gagnant
     */
    public boolean checkWinner() {
        // Test si un joueur a gagné
        boolean win = false;

        // 1 >> test les lignes
        for (int x = 0 ; x<3 ; x++) {
            if ((matrix[x][0] == matrix[x][1]) && (matrix[x][0]== matrix[x][2]) && (matrix[x][0] != this.noPlayer)) {
                win = true;
            }
        }

        // 2 >> test les colonnes
        for (int y = 0 ; y<3 ; y++) {
            if ((matrix[0][y] == matrix[1][y]) && (matrix[0][y] == matrix[2][y]) && (matrix[0][y] != this.noPlayer)) {
                win = true;
            }
        }

        // 3 >> test les diagonales
        if ((matrix[0][0] == matrix[1][1]) && (matrix[0][0] == matrix[2][2]) && (matrix[0][0] != this.noPlayer)) {
            win = true;
        }
        if ((matrix[0][2] == matrix[1][1]) && (matrix[0][2] == matrix[2][0]) && (matrix[0][2] != this.noPlayer)) {
            win = true;
        }

        return win;
    }

    /**
     * Vérifie si la grille est pleine (fin de partie sans gagnant)
     * @return      true si la grille est pleine
     */
    public boolean checkFull() {
        // Test si la grille est pleine
        return (nbrStrokes == 0);

        /*
        boolean full = true;
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                if (matrix[x][y] == this.noPlayer) {
                    full = false;
                }
            }
        }

        return full;*/
    }

}
